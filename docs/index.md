# PHPeriodic - the PHP Client for the Periodic API

These documents are for the PHP client interface to the Periodic API.

For full documentation of the Periodic API visit [periodic.is/api](http://periodic.is/api).

