<?php

namespace Periodic;

use \Periodic\Gateway;

class Client 
{
  
  private static $apiuser;
  private static $apikey;
  private static $gateway;
  private static $instance = null;

  public static function getInstance($apiuser=null, $apikey=null, $baseuri=null)
  {
    if(null == self::$instance){
      $myclass = __CLASS__;
      self::$instance = new $myclass;
    }

    if(!isset(self::$apiuser)){
      self::$apiuser = $apiuser;
    }

    if(!isset(self::$apikey)){
      self::$apikey = $apikey;
    }

    if(!isset(self::$gateway)){
      self::$gateway = Gateway::getInstance($baseuri);
    }

    return self::$instance;
  }

  protected function __construct()
  {
  }

  private function __clone()
  {
  }

  private function __wakeup()
  {
  }

  private function digest($input)
  {
    return hash_hmac('md5', $input, self::$apikey);
  }

  private function isAssociativeArray($arr) 
  {
    return array_keys($arr) !== range(0, count($arr) - 1);
  }

  private function object_helper($input) 
  {

    $accumulator = "";
    if(gettype($input) == 'object'){
      $asarray = get_object_vars($input);
    } else {
      $asarray = $input;
    }
    $mykeys = array_keys($asarray);
    sort($mykeys);
    foreach($mykeys as $k){
      $accumulator = $accumulator . "$k" . $this->serializeObject($asarray[$k]);
    }
    return $accumulator;

  }

  private function serializeObject($input)
  {

    $accumulator = "";

    switch (gettype($input)) {
      case 'array':
        if($this->isAssociativeArray($input)){
          $accumulator = $this->object_helper($input);
        } else {
          $holder = array();
          foreach($input as $item){
            $holder[] = $this->serializeObject($item);
          }
          $accumulator = implode(",", $holder);
        }
        break;
      case 'object':
        $accumulator = $this->object_helper($input);
        break;
      case 'boolean':
        $accumulator = ($input) ? 'true' : 'false';
      default:
        $accumulator = "$input";
        break;
    }

    return $accumulator;

  }

  private function serializeUrl($input) 
  {

    if(gettype($input) == 'object'){
      $input = get_object_vars($input);
    }

    $mykeys = array_keys($input);
    sort($mykeys);

    $accumulator = array();
    foreach($mykeys as $k){
      $accumulator[] = "$k/" . $this->serializeObject($input[$k]);
    }
    return implode("/", $accumulator);

  }

  private function addAuthHeader($payload, &$headers)
  {
    $payload = $this->digest($payload);
    $components = [ self::$apiuser, $payload ];
    $hash = implode("::", $components);
    $headers['WWW-Authenticate'] = $hash;
  }

  public function index($endpoint, $headers=array())
  {

    $this->addAuthHeader("/$endpoint", $headers);
    return self::$gateway->get($endpoint, ['headers' => $headers]);

  }

  public function retrieve($endpoint, $id, $headers=array())
  {

    $url = "$endpoint/id/$id";
    $this->addAuthHeader("/$url", $headers);

    return self::$gateway->get($url, ['headers' => $headers]);

  }

  public function create($endpoint, $body, $headers=array())
  {

    $hashinput = $this->serializeObject($body);
    $this->addAuthHeader($hashinput, $headers);

    $params = [
        'json' => $body,
        'headers' => $headers
      ];

    return self::$gateway->post($endpoint, $params);

  }

  public function update($endpoint, $id, $body, $headers=array())
  {

    $hashinput = $this->serializeObject($body);
    $this->addAuthHeader($hashinput, $headers);
    $url = "$endpoint/id/$id";

    $params = [
        'json' => $body,
        'headers' => $headers
      ];

    return self::$gateway->put($url, $params);

  }

  public function destroy($endpoint, $id, $headers=array())
  {

    $url = "$endpoint/id/$id";
    $this->addAuthHeader("/$url", $headers);

    return self::$gateway->delete($url, ['headers' => $headers]);

  }

  public function verifyhmac($input)
  {

    $body = $this->serializeObject($input);
    $digest = $this->digest($body);
    echo "DIGEST: $digest\n\n";
    echo "HASH: $body\n\n";
    $headers = array();
    $headers['WWW-Authenticate'] = self::$apiuser . '::';

    return self::$gateway->post('verifyhmac', ['json' => $input, 'headers' => $headers]);

  }

}
