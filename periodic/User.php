<?php

namespace Periodic;
use \Periodic\Entity;

class User extends Entity
{
  protected static $schema = [
      "authaccounts" => "object",
      "avatar" => "string",
      "firstname" => "string",
      "id" => "string",
      "lastname" => "string",
      "email" => "string",
      "slug" => "string",
      "apikey" => "string",
      "password" => "string",
      "address1" => "string",
      "address2" => "string",
      "city" => "string",
      "state" => "string",
      "zip" => "string",
      "resources" => "array",
      "role" => "string",
      "providers" => "array",
      "whitelabels" => "array",
      "type" => "string",
      "whitelabel" => "string"
    ];

  protected static $endpoint = 'user';

  public function __construct($content=null, $apiuser=null, $apikey=null, $baseuri=null)
  {

    parent::__construct($apiuser, $apikey, $baseuri);

    if($content){
      switch(gettype($content)){
        case "string":
          $this->id = $content;
          break;
        case "object":
          $this->extend($content);
          break;
        case "array":
          $this->extend($content);
          break;
        default:
          break;
      }
    }

  }

}
