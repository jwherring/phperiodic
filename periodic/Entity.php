<?php

namespace Periodic;
use \Periodic\Client;

class Entity
{
  private $client;
  protected $body;
  protected $id;
  protected $provider;

  public function __construct($apiuser=null, $apikey=null, $baseuri=null)
  {

    $this->body = new \stdClass();

    $this->client = Client::getInstance($apiuser, $apikey, $baseuri);

  }

  private function extendIfSet($body=null)
  {

    if(null !== $body){
      $this->extend($body);
    }

  }

  private function addProviderHeader(&$headers)
  {
    if(isset($this->provider)){
      $headers['x-periodic-provider'] = $this->provider;
    }
  }

  public function setId($id)
  {

    $this->id = $id;

  }

  private function getBodyAsObject($body)
  {

    $object = new \stdClass();

    foreach($body as $field => $value)
    {
      $object->$field = $value;
    }

    return $object;

  }

  public function extend($body)
  {

    switch(gettype($body)){
      case 'object':
        break;
      case 'array':
        $body = $this->getBodyAsObject($body);
        break;
      case 'string':
        $this->id = $body;
        $body = false;
        break;
      default:
        $body = false;
        break;
    }

    if($body){
      foreach(static::$schema as $field => $type)
      {
        if(isset($body->$field) && gettype($body->$field) == $type){
          $this->body->$field = $body->$field;
        }
      }
      if(isset($body->id)){
        $this->id = $body->id;
      }
      if(isset($body->provider)){
        $this->provider = $body->provider;
      }
      if(isset($body->subdomain)){
        $this->provider = $body->subdomain;
      }
    }

  }

  public function index($headers=array())
  {

    return $this->client->index(static::$endpoint, $headers);

  }

  public function create($body=null, $headers=array())
  {


    $this->extendIfSet($body);
    if(static::$endpoint != 'provider'){
      $this->addProviderHeader($headers);
    }
    $response = $this->client->create(static::$endpoint, $this->body, $headers);
    $bodyresponse = json_decode($response->getBody());
    $this->extend($bodyresponse);
    return $response;

  }

  public function retrieve($id=null, $headers=array())
  {
    
    $this->extendIfSet($id);
    $this->addProviderHeader($headers);
    $response = $this->client->retrieve(static::$endpoint, $this->id, $headers);
    $bodyresponse = json_decode($response->getBody());
    $this->extend($bodyresponse);
    return $response;

  }

  public function update($body=null, $headers=array())
  {

    $this->extendIfSet($body);
    $this->addProviderHeader($headers);
    print_r($this->body);
    //$response = $this->client->update(static::$endpoint, $this->id, $this->body, $headers);
    //$bodyresponse = json_decode($response->getBody());
    //$this->extend($bodyresponse);
    //return $response;

  }

  public function destroy($id=null, $headers=array())
  {

    $this->extendIfSet($id);
    $this->addProviderHeader($headers);
    $response = $this->client->destroy(static::$endpoint, $this->id, $headers);
    $bodyresponse = json_decode($response->getBody());
    $this->extend($bodyresponse);
    return $response;

  }

  public function verifyhmac()
  {
    $response = $this->client->verifyhmac($this->body);
    echo $response->getBody();
  }

}


