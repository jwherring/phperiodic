<?php

namespace Periodic;

class Gateway
{
  private static $instance;

  public static function getInstance($baseuri="http://periodic.is")
  {

    if(null == self::$instance){

      if(null == $baseuri){
        throw new \Exception('You must supply a base URI to instantiate a client');
      }

      self::$instance = new \GuzzleHttp\Client([
        'base_uri' => $baseuri,
        'http_errors' => false
      ]);

    }

    return self::$instance;

  }

  protected function __construct()
  {
  }

  private function __clone()
  {
  }

  private function __wakeup()
  {
  }

}
