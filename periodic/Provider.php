<?php

namespace Periodic;
use \Periodic\Entity;

class Provider extends Entity
{
  protected static $schema = [
      "active" => "boolean",
      "address" => "string",
      "agreementterms" => "string",
      "blackouts" => "array",
      "bookablerequestenabled" => "boolean",
      "bookingcss" => "string",
      "bookinglogo" => "string",
      "businesshours"  => "object",
      "city" => "string",
      "coordinates" => "array",
      "coverphoto" => "string",
      "custommessage" => "string",
      "datemessage" => "string",
      "description" => "string",
      "dictionary" => "object",
      "email" => "string",
      "name" => "string",
      "firstname" => "string",
      "lastname" => "string",
      "notifications" => "object",
      "orderofstages" => "array",
      "questions" => "array",
      "phone" => "string",
      "website" => "string",
      "facebook" => "string",
      "twitter" => "string",
      "instagram" => "string",
      "pinterest" => "string",
      "regions" => "array",
      "reservationtypes" => "array",
      "requirementgroups" => "array",
      "reservationsperbooking" => "integer",
      "resources" => "array",
      "state" => "string",
      "subdomain" => "string",
      "timemessage" => "string",
      "timezone" => "string",
      "userinstructions" => "string",
      "url" => "string",
      "zip" => "string",
      "users" => "array",
      "whitelabel" => "string"
    ];

  protected static $endpoint = 'provider';

  public function __construct($content=null, $apiuser=null, $apikey=null, $baseuri=null)
  {

    parent::__construct($apiuser, $apikey, $baseuri);

    if($content){
      switch(gettype($content)){
        case "string":
          $this->id = $content;
          break;
        case "object":
          $this->extend($content);
          break;
        case "array":
          $this->extend($content);
          break;
        default:
          break;
      }
    }

  }

}
